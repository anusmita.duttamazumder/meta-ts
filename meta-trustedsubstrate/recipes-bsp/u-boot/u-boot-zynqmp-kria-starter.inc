# Generate zynqmp-kria-starter kit style loader binaries
inherit deploy

SRC_URI += "file://zynqmp_fsbl.elf"
SRC_URI += "file://pmufw.elf"
SRC_URI += "file://som.its"
SRC_URI += "file://0001-som-Kria-specific-setting-configurations.patch"
SRC_URI += "file://0001-u-boot-zynqmp-Disable-the-hub-node-from-the-usb-nide.patch"
SRC_URI += "file://0001-zynqmp-add-reserved-nodes-for-firmware.patch"
SRC_URI += "file://0001-arm64-zynqmp-Introduce-kria-SOM-defconfig.patch"
SRC_URI += "file://primary.pem"
SRC_URI += "file://secondary.pem"

SRC_URI += "file://${MACHINE}.cfg"

COMPATIBLE_MACHINE = "zynqmp-kria-starter"

DEPENDS += "trusted-firmware-a optee-os"
DEPENDS += "xxd-native"
DEPENDS += "u-boot-mkimage-native bootgen-native"

UBOOT_BOARDDIR = "${S}/board/xilinx/zynqmp"
UBOOT_ENV_NAME = "zynqmp.env"

do_compile:prepend() {
    cp ${S}/../pmufw.elf \
        ${S}/../zynqmp_fsbl.elf \
        ${S}/som.its \
        ${RECIPE_SYSROOT}/firmware/bl31.elf \
        "${KCONFIG_CONFIG_ROOTDIR}"
    # FIXME we need to use tee-raw.bin, change this once https://github.com/OP-TEE/optee_os/pull/5830 gets merged
    cp ${RECIPE_SYSROOT}/${nonarch_base_libdir}/firmware/tee-pager_v2.bin "${KCONFIG_CONFIG_ROOTDIR}"
    cp ${S}/../*.pem "${KCONFIG_CONFIG_ROOTDIR}"
}

do_deploy:append() {
    mkdir -p "${DEPLOYDIR}"
    cd "${KCONFIG_CONFIG_ROOTDIR}"

    fdtoverlay -o zynqmp-smk-k26-revA-sck-kv-g-revA.dtb -i arch/arm/dts/zynqmp-smk-k26-revA.dtb arch/arm/dts/zynqmp-sck-kv-g-revA.dtbo
    fdtoverlay -o zynqmp-smk-k26-revA-sck-kv-g-revB.dtb -i arch/arm/dts/zynqmp-smk-k26-revA.dtb arch/arm/dts/zynqmp-sck-kv-g-revB.dtbo
    fdtoverlay -o zynqmp-sm-k26-revA-sck-kv-g-revA.dtb -i arch/arm/dts/zynqmp-sm-k26-revA.dtb arch/arm/dts/zynqmp-sck-kv-g-revA.dtbo
    fdtoverlay -o zynqmp-sm-k26-revA-sck-kv-g-revB.dtb -i arch/arm/dts/zynqmp-sm-k26-revA.dtb arch/arm/dts/zynqmp-sck-kv-g-revB.dtbo
    # Pack all combinations together
    mkimage -E -f som.its -B 0x8 fit-dtb.blob

    # Not needed, but keeping it for reference.  The generated sha3.txt
    # is what needs to be burned in the eFUSE for production use
    cat <<EOF > hash.bif
    generate_hash_ppk:
    {
    [pskfile] primary.pem
    [sskfile] secondary.pem
     [ bootloader,
       destination_cpu = a53-0,
       authentication = rsa
     ] zynqmp_fsbl.elf
    }
EOF

    bootgen -image hash.bif -arch zynqmp -w -o output_image.bin -efuseppkbits sha3.txt

    cat <<EOF > bootgen.bif
    the_ROM_image:
    {
    [fsbl_config] bh_auth_enable
    [auth_params] ppk_select=0
    [pskfile] primary.pem
    [sskfile] secondary.pem

    [ bootloader,
      destination_cpu=a53-0,
      authentication=rsa
    ] zynqmp_fsbl.elf
    [ destination_cpu=a53-0,
      exception_level=el-3,
      trustzone=secure,
      authentication=rsa
    ] bl31.elf
    [ destination_cpu = pmu,
     authentication = rsa
    ] pmufw.elf
    [ destination_cpu=a53-0,
      load=0x00100000,
      authentication=rsa
    ] fit-dtb.blob
    [ destination_cpu=a53-0,
      exception_level=el-2,
      authentication=rsa
    ] u-boot.elf
    [ destination_cpu=a53-0,
      load=0x60000000,
      startup=0x60000000,
      exception_level=el-1,
      trustzone=secure,
      authentication=rsa
    ] tee-pager_v2.bin
    }
EOF

    bootgen -image bootgen.bif -arch zynqmp -r -w -o xilinx_boot.bin

    cd -
    cp "${KCONFIG_CONFIG_ROOTDIR}/xilinx_boot.bin" "${DEPLOYDIR}/ImageA.bin"
    cp "${KCONFIG_CONFIG_ROOTDIR}/xilinx_boot.bin" "${DEPLOYDIR}/ImageB.bin"
}

ATF_DEPENDS = " trusted-firmware-a:do_deploy"
do_compile[depends] .= "${ATF_DEPENDS}"
