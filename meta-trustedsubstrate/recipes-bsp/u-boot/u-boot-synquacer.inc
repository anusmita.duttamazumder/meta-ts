FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/${MACHINE}:"

SRC_URI += "file://0001-board-synquacer-Update-the-flash-image-layout.patch"
SRC_URI += "file://0001-u-boot-synquacer-add-reserved-memory-nodes-for-optee.patch"
SRC_URI += "file://${MACHINE}.cfg"

UBOOT_BOARDDIR = "${S}/board/socionext/developerbox"
UBOOT_ENV_NAME = "developerbox.env"
