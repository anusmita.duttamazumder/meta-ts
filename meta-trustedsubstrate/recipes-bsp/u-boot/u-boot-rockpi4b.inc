# Generate Rockchip style loader binaries
inherit deploy

FILESEXTRAPATHS:prepend := "${THISDIR}/u-boot/${MACHINE}:"
# Rockpi is not a member.  A lot has changed since 2023.01 to -master
# keep it pinned until we find time to fix it
PV = "2023.01"
SRCREV = "62e2ad1ceafbfdf2c44d3dc1b6efc81e768a96b9"

SRC_URI += "file://${MACHINE}.cfg"
SRC_URI += "file://0001-phy-rockchip-inno-usb2-fix-hang-when-multiple-contro.patch"
SRC_URI += "file://0001-u-boot-rockpi4-add-optee-and-reserved-memory-nodes.patch"
SRC_URI += "file://0004-rk3399_common-add-dfu_alt_info-for-rockpi4b.patch"
SRC_URI += "file://0005-spl-fit-add-config-option-for-temporary-buffer-when-.patch"
SRC_URI += "file://0006-rockchip-rk3399-enable-spl-fifo-mode-for-sdmmc-only-.patch"
SRC_URI += "file://0007-rockchip-make_fit_atf-generate-signed-conf-when-FIT_.patch"
SRC_URI += "file://0009-rockchip-Enable-embedding-public-key-in-dtb.patch"
SRC_URI += "file://0010-arm-dts-rockpi4-change-rx_delay-for-gmac.patch"
SRC_URI += "file://0011-tpm2-ftpm-add-the-device-in-the-OP-TEE-services-list.patch"
SRC_URI += "file://0012-fix-guid-align.patch"
SRC_URI += "file://0001-tpm-add-a-function-that-performs-selftest-startup.patch"
SRC_URI += "file://0001-tpm-Add-tpm-autostart-shell-command.patch"
SRC_URI += "file://signature.dts"

SRC_URI:remove = "file://0001-event-Add-an-event-to-purge-DT-nodes-and-properties.patch"
SRC_URI:remove = "file://0002-dt-Provide-a-way-to-remove-non-compliant-nodes-and-p.patch"
SRC_URI:remove = "file://0003-fwu-Add-the-fwu-mdata-node-for-removal-from-devicetr.patch"
SRC_URI:remove = "file://0004-capsule-Add-the-signature-node-for-removal-from-devi.patch"
SRC_URI:remove = "file://0005-bootefi-Call-the-EVT_DT_NODE_PROP_PURGE-event-handle.patch"
SRC_URI:remove = "file://0006-doc-Add-a-document-for-non-compliant-DT-node-propert.patch"
SRC_URI:remove = "file://0007-test-event-Add-the-dt_purge-event-to-the-event-dump-.patch"
SRC_URI:remove = "file://0008-add-efi-https-boot.patch"
SRC_URI:remove = "file://0001-efi_loader-Make-DisconnectController-follow-the-EFI-.patch"

COMPATIBLE_MACHINE = "rockpi4b"

DEPENDS += "trusted-firmware-a gnutls-native"

UBOOT_BOARDDIR = "${S}/board/rockchip/evb_rk3399"
UBOOT_ENV_NAME = "evb_rk3399.env"

do_compile:prepend() {
	export BL31="${RECIPE_SYSROOT}/firmware/bl31.elf"
	ls -l ${BL31}
	export TEE="${RECIPE_SYSROOT}/lib/firmware/tee.bin"
	ls -l ${TEE}

	for config in ${UBOOT_MACHINE}; do
		mkdir -p uefi_capsule_certs
		tar xpvfz "${WORKDIR}/${UEFI_CAPSULE_CERT_FILE}" -C uefi_capsule_certs

		cp uefi_capsule_certs/CRT.pem ${B}/${config}/CRT.pem
		cp uefi_capsule_certs/CRT.pub.pem ${B}/${config}/CRT.pub.pem

		cp ${WORKDIR}/signature.dts ${B}/${config}/signature.dts
		export CAPSULE_SIG=${B}/${config}/signature.dts
		ESL="`pwd`/uefi_capsule_certs/CRT.esl"
		sed -i "s|CRT.esl|${ESL}|" ${CAPSULE_SIG}
	done
}

do_compile:append() {
	export FIT_SIGN_KEY=dev
	if [ "${FIT_SIGN_KEY}" ]; then {
		cd "${KCONFIG_CONFIG_ROOTDIR}"
		KEY="keys/${FIT_SIGN_KEY}.key"
		CRT="keys/${FIT_SIGN_KEY}.crt"
		mkdir -p keys
		[ -e "${KEY}" ] || \
			openssl genpkey -algorithm RSA -out "${KEY}" \
				-pkeyopt rsa_keygen_bits:2048 -pkeyopt rsa_keygen_pubexp:65537
		[ -e "${CRT}" ] || \
			openssl req -batch -new -x509 -key "${KEY}" -out "${CRT}"
		# Re-generate u-boot.its so that it contains signature nodes
		# make_fit_atf.py reads key name from ${FIT_SIGN_KEY}
		TEE=../../recipe-sysroot/lib/firmware/tee.bin BL31=../../recipe-sysroot/firmware/bl31.elf \
			../../git/arch/arm/mach-rockchip/make_fit_atf.py \
			arch/arm/dts/rk3399-rock-pi-4b.dtb > u-boot.its
		cp spl/dts/dt-spl.dtb spl/u-boot-spl.dtb
		# Generate signed u-boot.itb and add public key to spl/u-boot-spl.dtb
		./tools/mkimage -E -B 0x8 -p 0x0 -f u-boot.its -k keys \
			-r -K spl/u-boot-spl.dtb u-boot.itb
		# Generate idbloader.img which will authenticate u-boot.itb
		./tools/mkimage -n "rk3399" -T rksd -d tpl/u-boot-tpl.bin tpl/u-boot-tpl-rockchip.bin
		cat tpl/u-boot-tpl-rockchip.bin spl/u-boot-spl-nodtb.bin \
			spl/u-boot-spl.dtb > idbloader.img
	}; fi
}

do_deploy:append() {
	mkdir -p ${DEPLOYDIR}
	cp "${KCONFIG_CONFIG_ROOTDIR}/idbloader.img"  "${DEPLOYDIR}/idbloader.img"
	cp "${KCONFIG_CONFIG_ROOTDIR}/u-boot.itb" "${DEPLOYDIR}/u-boot.itb"
}

ATF_DEPENDS = " trusted-firmware-a:do_deploy"
do_compile[depends] .= "${ATF_DEPENDS}"
