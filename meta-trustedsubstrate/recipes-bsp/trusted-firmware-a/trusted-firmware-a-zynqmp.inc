# Xilinx kv260 and SOMs

COMPATIBLE_MACHINE:zynqmp-kria-starter = "zynqmp-kria-starter"
COMPATIBLE_MACHINE:zynqmp-zcu102 = "zynqmp-zcu102"

PV .= "+git${SRCREV_tfa}"

FILESEXTRAPATHS:prepend:zynqmp-kria-starter := "${THISDIR}/files/zynqmp-kria-starter:"
FILESEXTRAPATHS:prepend:zynqmp-zcu102 := "${THISDIR}/files/zynqmp-zcu102:"

# Get TF-A version
TFA_USED_VERSION = "${@bb.parse.vars_from_file(d.getVar('FILE', False),d)[1] or '1.0'}"

# For meta-arm nanbield (TF-A 2.9), include the patch
# For meta-arm master (TF-A 2.10), the patch is already upstreamed
SRC_URI:append:zynqmp-kria-starter = "${@bb.utils.contains("TFA_USED_VERSION", "2.9.0", \
                                      " file://0001-zynqmp-fix-limit-BL31_LIMIT.patch", \
                                      "", \
                                      d)}"

TFA_DEBUG = "0"
TFA_UBOOT = "0"
#TFA_MBEDTLS = "1"
TFA_BUILD_TARGET = "bl31"
TFA_INSTALL_TARGET = "bl31"
# Enabling Secure-EL1 Payload Dispatcher (SPD)
TFA_SPD:zynqmp-zcu102 = ""
TFA_SPD:zynqmp-kria-starter = "opteed"

TFA_TARGET_PLATFORM = "zynqmp"

EXTRA_OEMAKE:append:zynqmp-kria-starter = " ZYNQMP_CONSOLE=cadence1 \
					    ZYNQMP_ATF_MEM_BASE=0xfffea000 \
					    ZYNQMP_ATF_MEM_SIZE=0x16000 \
					    RESET_TO_BL31=1 LOG_LEVEL=0"

EXTRA_OEMAKE:append:zynqmp-zcu102 = " RESET_TO_BL31=1 "

do_deploy:append() {
    cp ${D}/firmware/bl31.bin ${DEPLOYDIR}
}

addtask deploy before do_build after do_compile
