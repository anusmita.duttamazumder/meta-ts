# IMX8 verdin machines specific TFA support

COMPATIBLE_MACHINE:imx8mp-verdin = "imx8mp-verdin"

PV .= "+git${SRCREV_tfa}"

FILESEXTRAPATHS:prepend:imx8mp-verdin := "${THISDIR}/files/imx8mp-verdin:"

TFA_DEBUG = "0"
TFA_UBOOT = "0"
TFA_MBEDTLS = "1"
TFA_BUILD_TARGET = "bl31"
TFA_INSTALL_TARGET = "bl31"
TFA_TARGET_PLATFORM = "imx8mp"

EXTRA_OEMAKE += "PLAT=${TFA_TARGET_PLATFORM} IMX_BOOT_UART_BASE=0x30880000"

addtask deploy before do_build after do_compile

FILES:${PN} = "/boot /firmware"

TOOLCHAIN = "gcc"
